from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from datetime import datetime, date

# Enter your name here
mhs_name = 'Imran Amrulloh' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 8, 14) #TODO Implement this, format (Year, Month, Date)
# Create your views here.
def index(request):
	if 'user_login' in request.session:
		response = {'name': mhs_name, 'age': calculate_age(birth_date.year)}
		return render(request, 'index_lab1.html', response)
	else:
		html = 'lab_9/session/login.html'
		return render(request, html, {})

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0