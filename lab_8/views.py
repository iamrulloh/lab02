from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
# Create your views here.
response = {}
def index(request):
	if 'user_login' in request.session:
		response['author'] = "Imran Amrulloh"
		html = 'lab_8/lab_8.html'
		return render(request, html, response)
	else:
		html = 'lab_9/session/login.html'
		return render(request, html, response)