from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.db.models.manager import Manager
from unittest.mock import patch
from .views import (
	index, add_friend, validate_npm, delete_friend,
	friend_list, get_friend_list, friend_description
)
from .models import Friend

import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class lab7UnitTest(TestCase):
	def test_lab_7_url_is_exist(self):
		response = self.client.get('/lab-7/')
		self.assertEqual(response.status_code,200)

		response = self.client.post('/lab-9/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})

		response = self.client.get('/lab-7/')
		self.assertEqual(response.status_code,200)

	def test_lab7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)

	def test_friend_list_url_is_exist(self):
		response = self.client.get('/lab-7/friend-list/')
		self.assertEqual(response.status_code, 200)

		response = self.client.post('/lab-9/custom_auth/login/', {'username': env("SSO_USERNAME"), 'password': env("SSO_PASSWORD")})

		response = self.client.get('/lab-7/friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_get_friend_list_data_url_is_exist(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_friend_description_url_is_exist(self):
		friend = Friend.objects.create(friend_name="Pina Korata", npm="1606123456")
		response = Client().post('/lab-7/friend-list/description/' + str(friend.npm) + '/')
		self.assertEqual(response.status_code, 200)

	def test_add_friend(self):
		response_post = Client().post(
			'/lab-7/add-friend/', 
			{'name':"imran", 'npm':"1606", 'alamat':" ", 'ttl':" ", 'prodi':" "}
		)
		self.assertEqual(response_post.status_code, 200)

	def test_validate_npm(self):
		response = self.client.post('/lab-7/validate-npm/')
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(html_response, {'is_taken':False})

	def test_delete_friend(self):
		friend = Friend.objects.create(friend_name="Pina Korata", npm="1606123456")
		response = Client().post('/lab-7/friend-list/delete-friend/' + str(friend.npm) + '/')
		self.assertEqual(response.status_code, 302)
		self.assertNotIn(friend, Friend.objects.all())